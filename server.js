const express = require('express');
const app = express();

const port = process.env.PORT || 8080;

app.use(express.static('./dist/angulardemo'));

app.get('/*', function (req, res) {
  res.sendFile('index.html', {root: 'dist/angulardemo/'}
  );
});

app.listen(port, () => {
  console.log('app is listening on port ' + port)
});
