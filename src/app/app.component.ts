import { Component } from '@angular/core';
import {ListComponent} from "./modules/shared/list/list.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angulardemo';

  constructor() { }

  ngOnInit(): void {
  }


}
