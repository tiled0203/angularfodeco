export interface Item{
  id?: number;
  title: string ;
  poster?: string;
  year: number;
  runtime?: number;
  rating: number;
  onlineId?: string;
}

