import {Item} from "./Item";
import {Injectable} from "@angular/core";
import {Adapter} from "./Adapter";

export interface Movie extends Item{
  plot?: string;
  seen?: boolean;
}

class MovieObj implements Movie{
  id: number;
  onlineId: string;
  plot: string;
  poster: string;
  rating: number;
  runtime: number;
  seen: boolean;
  title: string;
  year: number;


  constructor(id: number, onlineId: string, plot: string, poster: string, rating: number, runtime: number, seen: boolean, title: string, year: number) {
    this.id = id;
    this.onlineId = onlineId;
    this.plot = plot;
    this.poster = poster;
    this.rating = rating;
    this.runtime = runtime;
    this.seen = seen;
    this.title = title;
    this.year = year;
  }
}

@Injectable()
export class MovieAdapter implements Adapter<Movie>{
  adapt(item: any): Movie {
    return new MovieObj(
      item.id,
      item.onlineId,
      item.plot,
      item.poster,
      item.rating,
      item.runtime,
      item.seen,
      item.title,
      item.year);
  }

}
