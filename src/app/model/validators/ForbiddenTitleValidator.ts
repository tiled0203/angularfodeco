import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function forbiddenTitleValidator(titleRe: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const forbidden = titleRe.test(control.value);
    return forbidden ? {'forbiddenTitle': {value: control.value}} : null;
  }

}
