import {TvShowService} from "../../services/tv-show.service";
import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
import {map} from "rxjs/operators";

export function takenTitleValidator(service:TvShowService): ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null => {
    return service.checkTitleNotTaken(control.value).pipe(map(res => {
      return res.exists ? {titleTaken: true} : null;
    }));
  }

}

//Response
// {
//   exists: true
// }
