import {APP_INITIALIZER, NgModule, Optional, SkipSelf} from '@angular/core';
import {HomeComponent} from './home/home.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "../../services/auth-interceptor.service";
import {HeaderComponent} from "./header/header.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import {MatMenuModule} from "@angular/material/menu";
import {ConfigService} from "./config.service";
import {AppRoutingModule} from "../../app-routing.module";
import {LoginComponent} from './login/login.component';
import {LoginService} from "../../services/login.service";
import {AuthGuard} from "../../services/auth.guard";
import {MatSnackBar, MatSnackBarModule} from "@angular/material/snack-bar";
import {MovieService} from "../../services/movie.service";
import {TvShowService} from "../../services/tv-show.service";
import {CustomPreloadService} from "../../services/custom-preload.service";
import {MovieAdapter} from "../../model/Movie";


@NgModule({
  declarations: [
    HomeComponent, HeaderComponent, LoginComponent
  ],
  imports: [BrowserAnimationsModule, FormsModule, MatMenuModule, AppRoutingModule, MatSnackBarModule
  ],
  exports: [HeaderComponent, FormsModule],
  providers: [MovieAdapter, CustomPreloadService, MovieService, TvShowService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }
    , ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: (config: ConfigService) => () => config.load(),
      deps: [ConfigService],
      multi: true
    },
    LoginService,
    AuthGuard]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error("CoreModule is already loaded");
    }
  }
}
