import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ng-template-parent',
  templateUrl: './ng-template-parent.component.html',
  styleUrls: ['./ng-template-parent.component.css']
})
export class NgTemplateParentComponent implements OnInit {


  constructor() {
  }

  ngOnInit(): void {
  }

}
