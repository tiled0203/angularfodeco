import {Component, ViewChild} from '@angular/core';
import {ViewChildChildDemoComponent} from "../view-child-child-demo/view-child-child-demo.component";

@Component({
  selector: 'app-view-child-parent-demo',
  templateUrl: './view-child-parent-demo.component.html',
  styleUrls: ['./view-child-parent-demo.component.css']
})
export class ViewChildParentDemoComponent {
  @ViewChild(ViewChildChildDemoComponent)
  private numberComponent = {} as ViewChildChildDemoComponent;

  increase() {
    this.numberComponent.increaseByOne();
  }
  decrease() {
    this.numberComponent.decreaseByOne();
  }


}
