import {Component, OnDestroy, OnInit} from '@angular/core';
import {ExperimentService} from "../../../../services/experiment.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-experiment',
  templateUrl: './experiment.component.html',
  styleUrls: ['./experiment.component.css']
})
export class ExperimentComponent implements OnInit, OnDestroy {
  private subscription: Subscription | undefined;
  value: any;

  constructor(private experimentService: ExperimentService) {
  }

  ngOnInit(): void {
    // this.experimentService.test().toPromise().then(
    //   value => console.log(value)
    //   , error => {}
    // );
    // this.experimentService.switchMapDemo2();

    // this.zone.run(() => {
    //   setTimeout(() => {
    //     console.log('timeout callback is invoked.');
    //   });
    // });
    this.doAnAsyncOperation();
  }

  async doAnAsyncOperation() {
    let value1 = await this.resolveAfter2Seconds("value1").catch(reason => console.log(reason));
    this.value = value1;
    let value2 = await this.resolveAfter2Seconds("value2");
    this.value = value2;
  }

  resolveAfter2Seconds(x: any) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 5000);
    });
  }

  rejectAfter2Seconds(x: any) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        reject("rejected");
      }, 5000);
    });
  }

  ngOnDestroy(): void {
    // console.log("destroy!")
    // this.subscription?.unsubscribe();
  }

// Angular uses zones to enable automatic change detection in response to changes made by asynchronous tasks.
// more info can be found on https://angular.io/guide/zone
  zone = Zone.current.fork({
    name: 'zone',
    onScheduleTask: function (delegate, curr, target, task) {
      console.log('step2: new task is scheduled:', task.type, task.source);
      return delegate.scheduleTask(target, task);
    },
    onInvokeTask: function (delegate, curr, target, task, applyThis, applyArgs) {
      console.log('step4: task will be invoked:', task.type, task.source);
      return delegate.invokeTask(target, task, applyThis, applyArgs);
    },
    onHasTask: function (delegate, curr, target, hasTaskState) {
      console.log('step3: task state changed in the zone:', hasTaskState);
      return delegate.hasTask(target, hasTaskState);
    },
    onInvoke: function (delegate, curr, target, callback, applyThis, applyArgs) {
      console.log('step1: the callback will be invoked:', callback);
      return delegate.invoke(target, callback, applyThis, applyArgs);
    }
  });


}
