import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TvShowService} from "../../../../services/tv-show.service";
import {Router} from "@angular/router";
import {forbiddenTitleValidator} from "../../../../model/validators/ForbiddenTitleValidator";
import {takenTitleValidator} from "../../../../model/validators/TakenTitleValidator";

@Component({
  selector: 'app-tv-show-manual-add',
  templateUrl: './tv-show-manual-add.component.html',
  styleUrls: ['./tv-show-manual-add.component.css']
})
export class TvShowManualAddComponent implements OnInit {

  customError = {titleTaken: 'Sorry this title is already been taken!'}

  newTvShowForm: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private tvShowService: TvShowService, private router: Router) {
  }

  ngOnInit(): void {
    this.newTvShowForm = this.fb.group({
      title: [null, [Validators.required, Validators.minLength(2), forbiddenTitleValidator(/test/i)] , takenTitleValidator(this.tvShowService)],
      year: 10,
      runtime: 0,
      seasons: 0,
      episodes: 0,
      overview: [null, [Validators.required]],
      posterUrl: [null, [Validators.required]],
    })
  }

  addNewTvShow() {
    console.log(this.newTvShowForm.value);
    if(this.newTvShowForm.valid){
      this.tvShowService.saveNewTvShow(this.newTvShowForm.value).subscribe(tvShowResponse => this.router.navigate(["tvshow/detail", tvShowResponse.id]));
    }
  }
}
