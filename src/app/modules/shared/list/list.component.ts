import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MovieService} from "../../../services/movie.service";
import {Movie} from "../../../model/Movie";
import {Observable, Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {MovieAddComponent} from "../../features/movie/movie-add/movie-add.component";
import {TvShow} from "../../../model/TvShow";
import {Item} from "../../../model/Item";
import {TvShowAddComponent} from "../../features/tvshow/tv-show-add/tv-show-add.component";


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input() myItemList: Item[] = [];
  @Output() selected: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() delete: EventEmitter<Item> = new EventEmitter<Item>();
  @Input()
  showDelete = false;

  constructor(private activatedRoute: ActivatedRoute) {
    const currentComponent = activatedRoute.snapshot.component;
    this.showDelete = currentComponent !== MovieAddComponent && currentComponent !== TvShowAddComponent;
  }

  ngOnInit(): void {

  }

  deleteItem(item: Item) {
    this.delete.emit(item);
  }

  showDetail(item: Item) {
    this.activatedRoute.snapshot.data["movie"] = {};
    console.log("child sends ", item);
    this.selected.emit(item);
  }
}
