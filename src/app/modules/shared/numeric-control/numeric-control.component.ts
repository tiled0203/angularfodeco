import {Component, Input, ViewEncapsulation} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from "@angular/forms";

@Component({
  selector: 'app-numeric-control',
  templateUrl: './numeric-control.component.html',
  styleUrls: ['./numeric-control.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: NumericControlComponent
    }, {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: NumericControlComponent
    }
  ]
})
export class NumericControlComponent implements ControlValueAccessor, Validator {
  quantity = 0;
  @Input()
  label: string = "";
  @Input()
  increment: number = 1;

  onChange = (quantity: number) => {
  };

  onTouched = () => {
  };

  touched = false;

  disabled = false;


  onAdd() {
    this.markAsTouched();
    if (!this.disabled) {
      this.quantity += this.increment;
      this.onChange(this.quantity);
    }
  }

  onRemove() {
    this.markAsTouched();
    if (!this.disabled) {
      this.quantity -= this.increment;
      this.onChange(this.quantity);
    }
  }

  writeValue(quantity: number) { //parent form gives a value to this child control
    this.quantity = quantity;
  }

  registerOnChange(onChange: any) { // this child control gives a value back to the parent form, if the value of the child control changes
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) { // callback function to report touched status to parent form
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const quantity = control.value;
    if (quantity <= 0) {
      return {
        mustBePositive: {
          quantity
        }
      };
    }
    return null;
  }

}
