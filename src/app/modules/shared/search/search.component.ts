import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, NgForm} from "@angular/forms";
import {Item} from "../../../model/Item";
import {TvShow} from "../../../model/TvShow";
import {Movie} from "../../../model/Movie";
import {Subject} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, map} from "rxjs/operators";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() searchedItem: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  title: string = '';
  private titleSubject: Subject<string> = new Subject();

  constructor() {
  }

  ngOnInit(): void {
    this.titleSubject.pipe(
      debounceTime(1000)
      , distinctUntilChanged()
      , filter(value => {
        return value.length > 2 || value.length === 0
      }))
      .subscribe(value => this.searchedItem.emit(value));
  }

  search(value: string) {
    this.titleSubject.next(value);
  }
}
