import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {ListComponent} from "./list/list.component";
import {SearchComponent} from "./search/search.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DialogComponent} from "./dialog/dialog.component";
import {NgCircleProgressModule} from "ng-circle-progress";
import {NumericControlComponent} from "./numeric-control/numeric-control.component";
import {ControlErrorComponent} from "./control-error/control-error.component";
import {ControlErrorsDirective} from "./directives/control-error.directive";
import {FormSubmitDirective} from "./directives/form-submit.directive";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CoreModule} from "../core/core.module";
import {MatSnackBar} from "@angular/material/snack-bar";


@NgModule({
  declarations: [ListComponent, SearchComponent, DialogComponent, ControlErrorsDirective, NumericControlComponent, ControlErrorComponent, FormSubmitDirective],
  imports: [
    CommonModule, FormsModule, MatDialogModule, NgCircleProgressModule.forRoot({
      radius: 32,
      space: 2,
      toFixed: 1,
      outerStrokeWidth: 2,
      titleFontSize: "15",
      animateTitle: true,
      animationDuration: 1000,
      showSubtitle: false
    })
  ], exports: [CommonModule, ListComponent, SearchComponent, DialogComponent,
    MatIconModule,
    MatButtonModule,
    NgCircleProgressModule,
    NumericControlComponent,
    ControlErrorComponent,
    ControlErrorsDirective,
    FormSubmitDirective,
    ReactiveFormsModule
  ],
  entryComponents: [ControlErrorComponent],
  providers: []
})
export class SharedModule {
}
