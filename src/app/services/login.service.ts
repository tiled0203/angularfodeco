import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class LoginService {
  loggedIn: boolean = false;
  user: string = '';
  private userSubject: BehaviorSubject<any>;

  constructor() {
    this.userSubject = new BehaviorSubject<string>(JSON.parse(<string>localStorage.getItem('user')));
    this.userSubject.asObservable().subscribe(value => {
      this.loggedIn = true;
      this.user = value
    });
  }

  login(loginFormValues: any) {
    this.loggedIn = loginFormValues.password === loginFormValues.username;
    if (this.loggedIn) {
      this.user = loginFormValues.username;
      localStorage.setItem('user', JSON.stringify(this.user)); // NOT SAFE !
      this.userSubject.next(this.user);
    }
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.loggedIn = false;
  }
}
