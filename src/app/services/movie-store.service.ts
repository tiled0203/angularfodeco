import {Injectable} from '@angular/core';
import {Movie, MovieAdapter} from "../model/Movie";
import {BehaviorSubject} from "rxjs";
import {MovieSearchHistory} from "../model/MovieSearchHistory";

@Injectable()
export class MovieStoreService {
  private readonly _movieSource = new BehaviorSubject<Movie[]>([]);
  readonly _movies$ = this._movieSource.asObservable();
  searchTitle: string = '';

  constructor() {
  }

  // Get last value without subscribing to the _movies$ observable (synchronously).
  getMovies(): Movie[] {
    return this._movieSource.getValue();
  }

  setFoundMovies(movieHistory: MovieSearchHistory): void {
    console.log(movieHistory);
    this._movieSource.next(movieHistory.movies);
    this.searchTitle = movieHistory.title;
  }


}
