import {Injectable} from '@angular/core';
import {Movie, MovieAdapter} from "../model/Movie";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {map, switchMap, tap} from "rxjs/operators";

@Injectable()
export class MovieService {

  constructor(private http: HttpClient, private movieAdapter: MovieAdapter) {
  }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(environment.movieApiUrl).pipe(map((movies: Movie[]) => movies.map(value => this.movieAdapter.adapt(value))));
  }

  getMovie(id: number): Promise<Movie> {
    return this.http.get<Movie>(`${environment.movieApiUrl}/${id}`).pipe(map(value => this.movieAdapter.adapt(value))).toPromise();
  }

  lookupMovie(searchTitle: string, isOnline: boolean) {
    return this.http.get<Movie[]>(`${environment.movieApiUrl}/search`, {
      params: {
        title: searchTitle,
        online: isOnline
      }
    });
  }

  addMovie(onlineId: string | undefined) {
    return this.http.post<Movie>(`${environment.movieApiUrl}`, {"apiId": onlineId}).toPromise();
  }

  delete(id: number | undefined) {
    return this.http.delete(`${environment.movieApiUrl}/${id}`).toPromise();
  }
}
